@extends('layout.master')


@section('judul')
Halaman tambah cast
@endsection
@section('content')
<form action = '/cast' method = 'POST'>
    @csrf
    <div class="mb-3">
        <label for="nama" class="form-label">nama</label>
        <input type="text" name ="nama" class="form-control @error('nama') is-invalid @enderror" id="nama" >
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="mb-3">
        <label for="umur" class="form-label">umur</label>
        <input type="number" name= "umur" class="form-control @error('umur') is-invalid @enderror" id="umur">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="mb-3">
        <label for="bio" name = 'bio' class="form-label">Bio</label>
        <textarea class="form-control" name = 'bio' id="bio" rows="3"></textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
