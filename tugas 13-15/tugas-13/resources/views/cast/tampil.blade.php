@extends('layout.master')


@section('judul')
Data cast
@endsection
@section('content')
<a href="/cast/create" class = "btn btn-primary btn-sm" > Add</a>

<table class="table">
    <thead>
        <tr>
            <th scope="col">nomor</th>
            <th scope="col">Nama</th>
            <th scope="col"></th>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
    </thead>
    <tbody>
    @forelse ($cast as $key=>$value)
        <tr>
            <td>{{ $key + 1}}</td>
            <td>{{ $value->nama }}</td>
            <td><a class ="btn btn-info btn-sm" href="/cast/{{$value->id}}">detail</td>
            <td><a class ="btn btn-warning btn-sm" href="/cast/{{$value->id}}/edit">edit</a></td>
            <td>
                <form action= "/cast/{{$value->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>

    @empty
        <tr>
            <td>No data</td>
        </tr>

    @endforelse
    </tbody>
</table>
@endsection
