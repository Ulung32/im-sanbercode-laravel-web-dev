@extends('layout.master')


@section('judul')
Detail Cast
@endsection

@section('content')
<h1>{{$cast->nama}}</h1>
<p>umur: {{$cast->umur}}</p>
<p>bio: {{$cast->bio}}</p>
<a href="/cast" class="btn btn-secondary btm-sm">Back</a>
@endsection
