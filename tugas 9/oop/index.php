<?php 
        require_once('animal.php');
        require_once('Frog.php');
        require_once('Ape.php');

        $sheep = new Animal("shaun");

        echo "Name : " . $sheep->getName() . "<br>"; // "shaun"
        echo "legs : " . $sheep->getLegs() . "<br>"; // 4
        echo "cold blooded : " . $sheep->getColdBlooded() .  "<br><br>"; // "no"

        $kodok = new Frog("buduk");
        echo "Name : " . $kodok->getName() . "<br>"; 
        echo "legs : " . $kodok->getLegs() . "<br>"; 
        echo "cold blooded : " . $kodok->getColdBlooded() .  "<br>"; 
        echo "Jump : ";
        $kodok->jump() ; // "hop hop"
        echo "<br> <br>";
        
        $sungokong = new Ape("kera sakti");
        echo "Name : " . $sungokong->getName() . "<br>"; 
        echo "legs : " . $sungokong->getLegs() . "<br>"; 
        echo "cold blooded : " . $sungokong->getColdBlooded() .  "<br>";
        echo "Yell : ";
        $sungokong->yell(); // "Auooo"
        

?>
