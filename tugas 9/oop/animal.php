<?php
    class Animal{
        public $name;
        public $cold_blooded = 'no';
        public $legs = 4;

        public function __construct($string){
            $this->name = $string;
        }
        public function getName(){
            return $this->name;
        }
        public function setName($name){
            $this->name = $name;
        }
        public function getColdBlooded(){
            return $this->cold_blooded;
        }
        public function setColdBlooded($cold_blooded){
            $this->cold_blooded = $cold_blooded;
        }
        public function getLegs(){
            return $this->legs;
        }
        public function setLegs($legs){
            $this->legs = $legs;
        }
    }
