<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    function form(){
        return view('form');
    }

    function welcome(request $request){
        $firstName = $request->input('first-name');
        $lastName = $request->input('last-name');
        return view('welcome', ['firstName' => $firstName, 'lastName' => $lastName]);
    }
}
