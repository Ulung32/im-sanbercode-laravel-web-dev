<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name:</label><br><br>
        <input type="text" name ="first-name"><br><br>
        <label>Last Name:</label><br><br>
        <input type="text" name="last-name"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name ="gender">Male <br>
        <input type="radio" name ="gender">Female <br>
        <input type="radio" name ="gender">Other <br> <br>
        <label>Nationality:</label><br>
        <select name="Nationality">
            <option>Indonesia</option>
            <option>USA</option>
            <option>England</option>
        </select>
        <br><br>
        <label>Language Spoken: </label><br><br>
        <input type="checkbox">Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other<br>
        <br>
        <label>Bio: </label><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea>
        <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>
